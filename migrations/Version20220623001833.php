<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220623001833 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `customer` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `email` varchar(255) NOT NULL,
                `password` varchar(255) NOT NULL,
                `full_name` varchar(255) NOT NULL,
                `active` tinyint(1) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `idx_email` (`email`) USING BTREE,
                KEY `idx_active` (`active`) USING BTREE
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;"
        );

        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `item` (
                `id` INT(11) NOT NULL AUTO_INCREMENT ,
                `name` VARCHAR(255) NOT NULL ,
                `description` TEXT NOT NULL ,
                `price` DECIMAL(10, 2) NOT NULL ,
                `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                PRIMARY KEY (`id`) USING BTREE) ENGINE = InnoDB;"
        );

        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `order` (
                `id` INT(11) NOT NULL AUTO_INCREMENT ,
                `customer_id` INT(11) NOT NULL ,
                `delivery_address` VARCHAR(500) NOT NULL ,
                `billing_address` VARCHAR(500) NOT NULL ,
                `status` TINYINT UNSIGNED DEFAULT(1) NOT NULL ,
                `expected_time_of_delivery` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                PRIMARY KEY (`id`),
                KEY `idx_customer_id` (`customer_id`) USING BTREE) ENGINE = InnoDB;"
        );
        $this->addSql(
            "ALTER TABLE `order`
                ADD CONSTRAINT `order_customer_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE;"
        );

        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `order_item` (
                `id` INT(11) NOT NULL AUTO_INCREMENT ,
                `order_id` INT(11) NOT NULL ,
                `item_id` INT(11) NOT NULL ,
                `quantity` INT NOT NULL ,
                PRIMARY KEY (`id`),
                KEY `idx_order_id` (`order_id`),
                KEY `idx_item_id` (`item_id`) USING BTREE) ENGINE = InnoDB;"
        );
        $this->addSql(
            "ALTER TABLE `order_item` 
                    ADD CONSTRAINT `item_order_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
        ");
        $this->addSql(
            "ALTER TABLE `order_item` 
                    ADD CONSTRAINT `item_order_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
        ");

        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `delayed_order` (
                `id` INT(11) NOT NULL AUTO_INCREMENT ,
                `order_id` INT(11) NOT NULL ,
                `expected_time_of_delivery` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                `current_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                PRIMARY KEY (`id`),
                KEY `idx_order_id` (`order_id`) USING BTREE) ENGINE = InnoDB;"
        );
        $this->addSql(
            "ALTER TABLE `delayed_order`
                ADD CONSTRAINT `delayed_order_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;"
        );

    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY order_customer_ibfk_1');
        $this->addSql('ALTER TABLE `delayed_order` DROP FOREIGN KEY delayed_order_ibfk_1');
        $this->addSql('ALTER TABLE `order_item` DROP FOREIGN KEY item_order_ibfk_1');
        $this->addSql('ALTER TABLE `order_item` DROP FOREIGN KEY item_order_ibfk_2');

        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE `delayed_order`');
        $this->addSql('DROP TABLE `item`');
        $this->addSql('DROP TABLE `order_item`');
        $this->addSql('DROP TABLE `customer`');
    }
}

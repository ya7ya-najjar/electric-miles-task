<?php

namespace App\Service;

use App\Entity\Item;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class ItemService
{
    private $em;
    private $paginator;

    /**
     *
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     */
    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getItemList($page = 1, ?array $filter = []): PaginationInterface
    {
        $filter = new ArrayCollection($filter);
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('i')
            ->from(Item::class, 'i')
            ->where('1 = 1');

        if($filter->get('search')) {
            $qb->andWhere('i.name LIKE :search');
            $qb->setParameter('search', $filter->get('search'));
        }

        $query = $qb->getQuery();
        return $this->paginator->paginate(
            $query,
            $page,
            10
        );
    }
}
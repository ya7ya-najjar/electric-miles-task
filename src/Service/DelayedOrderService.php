<?php

namespace App\Service;

use App\Entity\DelayedOrder;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class DelayedOrderService
{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getDelayedOrderList(Request $request, $page = 1): PaginationInterface
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('d')
            ->from(DelayedOrder::class, 'd')
            ->where('1 = 1');

        if($request->get('start_time')) {
            $qb->andWhere('d.currentTime > :start');
            $qb->setParameter('start', $request->get('start_time'));
        }

        if($request->get('end_time')) {
            $qb->andWhere('d.currentTime < :end');
            $qb->setParameter('end', $request->get('end_time'));
        }

        $query = $qb->getQuery();
        return $this->paginator->paginate(
            $query,
            $page,
            10
        );
    }
}
<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\Order;
use App\Entity\OrderItem;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class OrderService
{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getOrderList(Request $request, $page = 1): PaginationInterface
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('o')
            ->from(Order::class, 'o')
            ->leftJoin('o.customer', 'c')
            ->where('1 = 1');

        if($request->get('customer')) {
            $qb->andWhere('c.id = :customer');
            $qb->setParameter('customer', $request->get('customer'));
        }

        if($request->get('order')) {
            $qb->andWhere('o.id = :order');
            $qb->setParameter('order', $request->get('order'));
        }

        if($request->get('status')) {
            $qb->andWhere('o.status = :status');
            $qb->setParameter('status', $request->get('status'));
        }

        $query = $qb->getQuery();
        return $this->paginator->paginate(
            $query,
            $page,
            10
        );
    }

    public function addItemsToOrder($items, Order $order)
    {
        foreach ($items as $item)
        {
            $qty = $item['quantity'] ?? 1;
            if (isset($item['id'])){
                $orderItem = new OrderItem();
                $item = $this->em->getRepository(Item::class)->find($item['id']);
                if ($item){
                    $orderItem->setItem($item)
                        ->setOrder($order)
                        ->setQuantity($qty);
                    $this->em->persist($orderItem);
                    $this->em->flush();
                }
            }
        }
    }

    public function updateOrderItems($items, Order $order)
    {
        //removing order items which are no longer exist
        $existingItems = $order->getItems();
        /** @var OrderItem $existingItem */
        foreach ($existingItems as $existingItem) {
            $found = false;
            foreach ($items as $item) {
                if($existingItem->getItem()->getId() == $item) {
                    $existingItem->setQuantity($item['quantity']??1);
                    $found = true;
                }
            }
            if(!$found) {
                $this->em->getRepository(OrderItem::class)->remove($existingItem);
            }
        }

        //adding new items
        foreach ($items as $item) {
            $qty = $item['quantity'] ?? 1;
            if(!$order->orderItemExists($item['id']) && isset($item['id'])) {
                $orderItem = new OrderItem();
                $item = $this->em->getRepository(Item::class)->find($item['id']);
                if ($item){
                    $orderItem->setItem($item)
                        ->setOrder($order)
                        ->setQuantity($qty);
                    $this->em->persist($orderItem);
                    $this->em->flush();
                }
            }
        }
    }
}
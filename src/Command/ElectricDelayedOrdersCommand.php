<?php

namespace App\Command;

use App\Entity\Order;
use App\Repository\DelayedOrderRepository;
use App\Repository\OrderRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ElectricDelayedOrdersCommand extends Command
{
    protected static $defaultName = 'electric:delayed-orders';
    protected static $defaultDescription = 'Verify if there are delayed orders then add them to the Delayed Order table';

    private $orderRepository;
    private $delayedOrderRepository;
    public function __construct(string $name = null, OrderRepository $orderRepository, DelayedOrderRepository $delayedOrderRepository)
    {
        parent::__construct($name);
        $this->orderRepository = $orderRepository;
        $this->delayedOrderRepository = $delayedOrderRepository;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->note(sprintf('Start looking for delayed orders'));

        $delayedOrders = $this->orderRepository->getDelayedOrders();

        /** @var Order $item */
        foreach ($delayedOrders as $delayedOrder){
            $delayedOrder->setStatus(Order::STATUS_DELAYED);
            $this->orderRepository->save($delayedOrder);
            $this->delayedOrderRepository->createDelayedOrder($delayedOrder);
        }

        $io->success('Delayed Orders Saved!');

        return Command::SUCCESS;
    }
}

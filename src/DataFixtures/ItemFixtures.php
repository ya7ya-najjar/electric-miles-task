<?php

namespace App\DataFixtures;

use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ItemFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create();

        for ($i = 0; $i <= 9; $i++) {
            $item = new Item();
            $item->setName($this->faker->name)
                ->setDescription($this->faker->realText())
                ->setPrice($this->faker->numberBetween(20, 200));

            $manager->persist($item);
        }

        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CustomerFixtures extends Fixture
{
    protected $faker;
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create();

        for ($i = 0; $i <= 3; $i++) {
            $customer = new Customer();
            $hashedPassword = $this->passwordHasher->hashPassword($customer, '123');

            $customer
                ->setActive(1)
                ->setEmail($this->faker->email)
                ->setFullName($this->faker->name)
                ->setPassword($hashedPassword);

            $manager->persist($customer);
        }

        $manager->flush();
    }
}

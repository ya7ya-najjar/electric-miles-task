<?php

namespace App\Entity;

use App\Repository\DelayedOrderRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="`delayed_order`")
 * @ORM\Entity(repositoryClass=DelayedOrderRepository::class)
 * @Serializer\ExclusionPolicy("all")
 */
class DelayedOrder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DateTime
     * @Serializer\Expose()
     * @ORM\Column(name="expected_time_of_delivery", type="datetime", nullable=false)
     */
    private $expectedTimeOfDelivery;

    /**
     * @var DateTime
     * @Serializer\Expose()
     * @ORM\Column(name="`current_time`", type="datetime", nullable=false)
     */
    private $currentTime;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     * @Serializer\Expose()
     */
    private $order;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getExpectedTimeOfDelivery(): DateTime
    {
        return $this->expectedTimeOfDelivery;
    }

    /**
     * @param DateTime $expectedTimeOfDelivery
     * @return DelayedOrder
     */
    public function setExpectedTimeOfDelivery(DateTime $expectedTimeOfDelivery): self
    {
        $this->expectedTimeOfDelivery = $expectedTimeOfDelivery;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCurrentTime(): DateTime
    {
        return $this->currentTime;
    }

    /**
     * @param DateTime $currentTime
     * @return DelayedOrder
     */
    public function setCurrentTime(DateTime $currentTime): self
    {
        $this->currentTime = $currentTime;
        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return DelayedOrder
     */
    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="`order`")
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{
    const STATUS_PENDING = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_ONGOING = 3;
    const STATUS_DELIVERED = 4;
    const STATUS_DELAYED = 5;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_address", type="string", length=500, nullable=false)
     * @Assert\NotBlank()
     * @Serializer\Expose()
     */
    private $delivery_address;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address", type="string", length=500, nullable=false)
     * @Assert\NotBlank()
     * @Serializer\Expose()
     */
    private $billing_address;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     * @Assert\GreaterThan(1)
     * @Serializer\Expose()
     */
    private $status;

    /**
     * @var DateTime
     * @Serializer\Expose()
     * @ORM\Column(name="`expected_time_of_delivery`", type="datetime", nullable=false)
     */
    private $expectedTimeOfDelivery;

    /**
     * @var DateTime
     * @Serializer\Expose()
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     * @Serializer\Expose()
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @Serializer\Expose()
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     *
     */
    private $customer;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="order")
     * @Serializer\Expose()
     * @Assert\Count(
     *  min="0",
     *  minMessage="There should be 1 item at least"
     * )
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getDeliveryAddress(): string
    {
        return $this->delivery_address;
    }

    /**
     * @param string $delivery_address
     * @return Order
     */
    public function setDeliveryAddress(string $delivery_address): self
    {
        $this->delivery_address = $delivery_address;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddress(): string
    {
        return $this->billing_address;
    }

    /**
     * @param string $billing_address
     * @return Order
     */
    public function setBillingAddress(string $billing_address): self
    {
        $this->billing_address = $billing_address;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpectedTimeOfDelivery(): DateTime
    {
        return $this->expectedTimeOfDelivery;
    }

    /**
     * @param string $expectedTimeOfDelivery
     * @return Order
     * @throws Exception
     */
    public function setExpectedTimeOfDelivery(string $expectedTimeOfDelivery): self
    {
        $this->expectedTimeOfDelivery = new DateTime($expectedTimeOfDelivery);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return Order
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer|null $customer
     * @return Order
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|OrderItem[]|null
     */
    public function getItems(): ?Collection
    {
        $arrayCollection = new ArrayCollection();
        foreach ($this->items as $item){
            $arrayCollection->add($item);
        }
        return $arrayCollection;
    }

    public function orderItemExists($id): bool
    {
        /** @var OrderItem $item */
        foreach ($this->getItems() as $item) {
            if($item->getId() == $id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("statusName")
     */
    public function getStatusName(): string
    {
        switch ($this->status){
            case self::STATUS_IN_PROGRESS:
                return 'In Progress';
            case self::STATUS_ONGOING:
                return 'On Going';
            case self::STATUS_DELIVERED:
                return 'Delivered';
            case self::STATUS_DELAYED:
                return 'Delayed';
            default:
                return 'Pending';
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function beforeUpdate()
    {
        $this->setUpdatedAt(new DateTime());
    }

    /**
     * @ORM\PrePersist()
     */
    public function beforeAdd()
    {
        $now = new DateTime();
        $this->setCreatedAt($now);
        $this->setUpdatedAt($now);
        $this->setStatus(self::STATUS_PENDING);
    }
}

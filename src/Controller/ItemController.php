<?php

namespace App\Controller;

use App\Service\ItemService;
use App\Service\RestHelperService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use OpenApi\Annotations as OA;

/**
 * Items controller.
 * @Rest\Route("/api/items")
 */
class ItemController extends AbstractFOSRestController
{
    private $itemService;
    private $rest;

    public function __construct(ItemService $itemService, RestHelperService $rest)
    {
        $this->itemService = $itemService;
        $this->rest = $rest;
    }

    /**
     * Lists all Items.
     * @Rest\Get("/")
     * @OA\Response(
     *     response=200,
     *     description="Returns the Items"
     * )
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Tag(name="Items")
     */
    public function index(Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $filter = (array) $request->request->get('filter');
        $pagination = $this->itemService->getItemList($page, $filter);
        $this->rest->setPagination($pagination);

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }
}
<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderType;
use App\Service\OrderService;
use App\Service\RestHelperService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Orders controller.
 * @Rest\Route("/api")
 */
class OrderController extends AbstractFOSRestController
{
    private $em;
    private $orderService;
    private $rest;

    public function __construct(EntityManagerInterface $em, OrderService $orderService, RestHelperService $rest)
    {
        $this->em = $em;
        $this->orderService = $orderService;
        $this->rest = $rest;
    }

    /**
     * Lists all Orders.
     * @Rest\Get("/orders")
     * @OA\Response(
     *     response=200,
     *     description="Returns the Orders"
     * )
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="status",
     *     in="query",
     *     example=2,
     *     description="Order Status",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Tag(name="Orders")
     */
    public function index(Request $request): Response
    {
        $page = $request->get('page', 1);
        $pagination = $this->orderService->getOrderList($request, $page);
        $this->rest->setPagination($pagination);

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }

    /**
     * Get Order.
     * @Rest\Get("/orders/{id}")
     * @OA\Tag(name="Orders")
     * @param Order $order
     * @return Response
     */
    public function getOrder(Order $order): Response
    {
        return $this->handleView($this->view($order));
    }

    /**
     * Create Order.
     * @Rest\Post("/orders")
     * @OA\RequestBody(
     *      description="Add New Order",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              example={
     *                      "delivery_address": "Lebanon, Beirut",
     *                      "billing_address": "Lebanon, Beirut",
     *                      "expected_time_of_delivery": "2022-06-24 19:11",
     *                      "items": {
     *                          {"id": 1, "quantity": 2},
     *                          {"id": 2, "quantity": 2},
     *                          {"id": 3, "quantity": 4},
     *                      },
     *                      "customer": 17,
     *                      "status": 1
     *              }
     *          )
     *      )
     * )
     * @OA\Tag(name="Orders")
     * @param Request $request
     * @return Response
     */
    public function addOrder(Request $request): Response
    {
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($order);
            $this->em->flush();

            $this->orderService->addItemsToOrder($request->get('items'), $order);

            $this->em->refresh($order);
            $this->rest->setData($order);
            return $this->handleView(
                $this->view($this->rest->getResponse(), Response::HTTP_CREATED)
            );
        }
        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView($this->view($this->rest->getResponse()));
    }

    /**
     * Update Order.
     * @Rest\Put("/orders/{id}")
     * @OA\RequestBody(
     *      description="Update Order",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              example={
     *                      "delivery_address": "Lebanon, Beirut",
     *                      "billing_address": "Lebanon, Beirut",
     *                      "expected_time_of_delivery": "2022-06-24 19:11",
     *                      "items": {
     *                          {"id": 1, "quantity": 2},
     *                          {"id": 2, "quantity": 2},
     *                          {"id": 3, "quantity": 4},
     *                      },
     *                      "customer": 17,
     *                      "status": 1
     *              }
     *          )
     *      )
     * )
     * @OA\Tag(name="Orders")
     * @param Request $request
     * @param Order $order
     * @return Response
     */
    public function editOrder(Request $request, Order $order): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($order);
            $this->em->flush();

            $this->orderService->updateOrderItems($request->get('items'), $order);

            $this->em->refresh($order);
            $this->rest->setData($order);
            return $this->handleView(
                $this->view($this->rest->getResponse(), Response::HTTP_CREATED)
            );
        }
        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView($this->view($this->rest->getResponse()));
    }
}

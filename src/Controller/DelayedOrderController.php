<?php

namespace App\Controller;

use App\Service\DelayedOrderService;
use App\Service\RestHelperService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use OpenApi\Annotations as OA;

/**
 * DelayedOrders controller.
 * @Rest\Route("/api/delayd-orders")
 */
class DelayedOrderController extends AbstractFOSRestController
{
    private $itemService;
    private $rest;

    public function __construct(DelayedOrderService $itemService, RestHelperService $rest)
    {
        $this->itemService = $itemService;
        $this->rest = $rest;
    }

    /**
     * Lists all Delayed Orders.
     * @Rest\Get("/")
     * @OA\Response(
     *     response=200,
     *     description="Returns the Delayed Orders"
     * )
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="start_time",
     *     in="query",
     *     example="2022-06-24 19:11",
     *     description="Start Time",
     *     @OA\Schema(type="string")
     * )
     *
     * @OA\Parameter(
     *     name="end_time",
     *     in="query",
     *     example="2022-06-26 19:11",
     *     description="End Time",
     *     @OA\Schema(type="string")
     * )
     *
     * @OA\Tag(name="DelayedOrders")
     */
    public function index(Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $pagination = $this->itemService->getDelayedOrderList($request, $page);
        $this->rest->setPagination($pagination);

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }
}

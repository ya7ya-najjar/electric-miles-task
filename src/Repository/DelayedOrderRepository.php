<?php

namespace App\Repository;

use App\Entity\DelayedOrder;
use App\Entity\Order;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DelayedOrder>
 *
 * @method DelayedOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method DelayedOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method DelayedOrder[]    findAll()
 * @method DelayedOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DelayedOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DelayedOrder::class);
    }

    public function add(DelayedOrder $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(DelayedOrder $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function createDelayedOrder(Order $order)
    {
        $delayedOrder = new DelayedOrder();
        $delayedOrder
            ->setOrder($order)
            ->setExpectedTimeOfDelivery($order->getExpectedTimeOfDelivery())
            ->setCurrentTime(new DateTime());

        $this->add($delayedOrder, true);
    }
}

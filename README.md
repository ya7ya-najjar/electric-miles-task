# Electric Miles Task using Symfony 5.4

### Introduction

Ordering Api With Swagger Documentation


### Installation

1. Open in cmd or terminal app and navigate to this folder
2. Run following commands

```bash
composer install
```

```bash
cp .env .env.dev
```

```bash
php bin/console doctrine:database:create
```

```bash
php bin/console doctrine:migration:migrate
```

```bash
 php bin/console doctrine:fixtures:load
```

```bash
symfnoy server:start --allow-http
```

To check if we have any delayed orders please run the command 
```bash
php bin/console electric:delayed-orders
```

To run tests
```bash
php bin/phpunit
```
To check Swagger API Documentation navigate to url

http://localhost:8000/api/doc

### Yahya Alnajjar
Full Stack Developer

...

<?php

namespace App\Tests;

use App\Controller\OrderController;
use App\Entity\Order;
use App\Repository\CustomerRepository;
use App\Repository\OrderRepository;
use App\Service\OrderService;
use App\Service\RestHelperService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderControllerTest extends KernelTestCase
{

    protected $orderController;
    protected $em;
    protected $orderService;
    protected $rest;
    protected $paginator;
    protected $pagination;
    protected $request;
    protected $jsonResponse;
    protected $customerRepository;
    protected $orderRepository;
    protected $response;
    protected $viewHandler;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->orderService = $this->createMock(OrderService::class);
        $this->rest= $this->createMock(RestHelperService::class);
        $this->paginator = $this->createMock(PaginatorInterface::class);
        $this->pagination = $this->createMock(PaginationInterface::class);
        $this->customerRepository = $this->createMock(CustomerRepository::class);
        $this->orderRepository = $this->createMock(OrderRepository::class);
        $this->request = $this->createMock(Request::class);
        $this->viewHandler = $this->createMock(ViewHandlerInterface::class);

        $this->jsonResponse = new JsonResponse();
        $this->response = new Response();

        self::bootKernel();
        $container = self::$kernel->getContainer();
        $container->set('fos_rest.view_handler', $this->viewHandler);

        $this->orderController = new OrderController($this->em, $this->orderService, $this->rest);
        $this->orderController->setContainer($container);
    }

    public function testIndex(): void
    {
        $this->paginator->expects($this->any())->method('paginate')->willReturn($this->pagination);
        $this->orderService->method('getOrderList')->willReturn($this->pagination);

        $response = [
            "success" => true,
            "pagination" => [
                "page" => 1,
                "pages" => 0,
                "totalItems" => 0,
                "items" => []
            ]
        ];

        $this->jsonResponse->setData($response);
        $this->request->method('get')->willReturn(1);
        $this->viewHandler->expects($this->any())->method('handle')->willReturn($this->jsonResponse);

        $result = $this->orderController->index($this->request);
-
        $this::assertEquals($this->jsonResponse->getContent(), $result->getContent());
        $this->assertTrue(true);
    }

    public function testAddOrder(): void
    {
        $customer = $this->customerRepository->find(1);

        $order = new Order();

        $order
            ->setExpectedTimeOfDelivery("2022-06-24 19:11")
            ->setDeliveryAddress("Lebanon, Beirut")
            ->setBillingAddress("Lebanon, Beirut")
            ->setStatus(Order::STATUS_PENDING)
            ->setCustomer($customer);

        $this->em->method('persist')->willReturn(null);
        $this->em->method('flush')->willReturn(null);

        $this->assertEquals(null, $this->orderRepository->save($order));
    }
}
